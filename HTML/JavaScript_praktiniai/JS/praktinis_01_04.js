"use strict";

function changeToColor() {
    var img = document.getElementById("demo");
    img.setAttribute("class", "color");
}

function changeToGrayscale() {
    var img = document.getElementById("demo");
    img.setAttribute("class", "grayscale");
}
