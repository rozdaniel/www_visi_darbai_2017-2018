"use strict";

function positionLeft() {
    var src = document.getElementById("demo2");
    src.setAttribute("class", "hidden");
    var img = document.getElementById("demo1");
    img.setAttribute("class", "left");
}

function positionRight() {
    var src = document.getElementById("demo2");
    src.setAttribute("class", "hidden");
    var img = document.getElementById("demo1");
    img.setAttribute("class", "right");
}

function invisible() {
    var src = document.getElementById("demo2");
    src.setAttribute("class", "hidden");
    var img = document.getElementById("demo1");
    img.setAttribute("class", "hidden");
}

function reset() {
    location.reload(true);
}

function myFunction1() {
    var img = document.getElementById("demo1");
    img.setAttribute("class", "hidden");
    var src = document.getElementById("demo2");
    src.setAttribute("class", "image")
}
