"use strict";
var bookDisplayArea = document.getElementById("knygynas");

function Book(
    pavadinimas,
    autorius,
    leidimoMetai,
    puslapiuSk,
    knyguLikutis,
    kaina,
    img,
) {
    this.pavadinimas = pavadinimas;
    this.autorius = autorius;
    this.leidimoMetai = leidimoMetai;
    this.puslapiuSk = puslapiuSk;
    this.knyguLikutis = knyguLikutis;
    this.kaina = kaina
    this.img = img;
}

var books = [
    new Book("Stebuklingas tolumų medis", "Enid Blyton", "2016", "456", "23", "13.46", "../../IMG/tolumu_medis.png"),
    new Book("Pagaliuku šuo", "Tom Watson", "2014", "192", "3", "6.83", "../../IMG/pagaliuku_suo.jpg"),
    new Book("Puokiu istorijos", "Edgar Valter", "2014", "180", "12", "11.96", "../../IMG/puokiu_istorijos.jpg"),
];

function getBookDiv(books) {
    return "<div class=\"grid_container\"><div><img class=\"img\" src=" + books.img + " alt=\"book\"/></div><div class\"box1\"><h2>\"" + books.pavadinimas + "\"</h2><h3>Bye " + books.autorius + "</h3><p>Leidimo metai: " + books.leidimoMetai + " m.</p><p>Psl.skaičius: " + books.puslapiuSk + " psl.</p><p>Likutis: " + books.knyguLikutis + " vnt.</p><p>Kaina: " + books.kaina + " €</p></div></div>";
}

function showAll() {
    bookDisplayArea.innerHTML = "";
    for (var x in books) {
        bookDisplayArea.innerHTML += getBookDiv(books[x]);
    }
}

var inp = document.getElementsByName("search");

function autorius() {
    alert("function autorius()");
    for(var x in inp){
        if (inp[0].value == books[x].autorius) {
            alert("Toks autorius yra.");
            document.getElementById("result").innerHTML = "Toks autorius yra.";
        } else {
            alert("Tokio autoriaus nera.");
            document.getElementById("result").innerHTML = "Tokio autoriaus nera.";
        }
    }
}
function pavadinimas() {
    alert("function pavadinimas()");
    for(var x in inp){
        if (inp[1].value == books[x].pavadinimas) {
            alert("Knyga su tokių pavadinimų yra.");
            document.getElementById("result").innerHTML = "Knyga su tokių pavadinimų yra.";
        } else {
            alert("Knygos su tokių pavadinimų nėra.");
            document.getElementById("result").innerHTML = "Knygos su tokių pavadinimų nėra.";
        }
    }
}

document.body.onload = showAll();