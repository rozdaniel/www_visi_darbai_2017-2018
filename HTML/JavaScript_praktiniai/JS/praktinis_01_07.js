"use strict";

function sudeti() {
    var sk1 = document.getElementById("firstNumber").value;
    var sk2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = Number(sk1) + Number(sk2);
}

function atimti() {
    var sk1 = document.getElementById("firstNumber").value;
    var sk2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = Number(sk1) - Number(sk2);
}

function padauginti() {
    var sk1 = document.getElementById("firstNumber").value;
    var sk2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = Number(sk1) * Number(sk2);
}

function padalinti() {
    var sk1 = document.getElementById("firstNumber").value;
    var sk2 = document.getElementById("secondNumber").value;
    if (sk2 != 0 && sk1 != 0) {
        document.getElementById("result").innerHTML = Number(sk1) / Number(sk2);
    } else {
        alert("Įvedams pirmas it antras skaičius negali būti 0");
    }
}
