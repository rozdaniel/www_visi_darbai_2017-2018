"use strict";

function changeSizeBigger() {
    var img = document.getElementById("demo");
    img.setAttribute("class", "big");
}

function changeSizeSmaller() {
    var img = document.getElementById("demo");
    img.setAttribute("class", "small");
}
