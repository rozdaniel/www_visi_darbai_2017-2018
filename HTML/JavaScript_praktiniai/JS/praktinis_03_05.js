"use strict";

//1. Suskaičiuoti kiek HTML dokumente yra nuorodų (<a>);

function myA() {
    var a = document.getElementsByTagName("a");
    //alert("Nuorodų skaičius: " + a.length);
    document.getElementById("ats").innerHTML = a.length + " nuorodos";
}

//2. Pakeisti visų antraščių (<h1>) spalvą ir šrifto dydį;

function myH() {
    alert("myH");
    var h = document.getElementsByTagName("h1");
    for (i = 0; i < h.length; i++) {
        h[i].style.color = "red";
        h[i].style.fontSize = "3.0em";
    }
}

//3. Visoms pastraipoms, esančioms pagrindiniame div bloke
//(id="maincol") pakeisti teksto spalvą, priklausomai nuo
//indekso, lyginis – žalia, nelyginis – mėlyna. Panaudoti kelis
//skirtingus „paėmimo“ būdus.

function myDoubleP() {
    alert("myDoubleP");
    myP();
    myP2();
}

function myP() {
    var m = document.getElementById("maincol");
    var p = m.getElementsByTagName("p");
    var e = 0;
    while (e <= p.length) {
        p[e].style.color = "blue";
        e += 2;
    }
}

function myP2() {
    var m = document.getElementById("maincol");
    var p = m.getElementsByTagName("p");
    var e = 1;
    while (e <= p.length) {
        p[e].style.color = "green";
        e += 2;
    }
}
